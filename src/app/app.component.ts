import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private _btnPlay: string ='Démarrer' ;
  private _timer=new Timer();
  private _state=new State();
  play() {
    this._timer. start();
    this._state. setPlay();
    this._btnPlay = 'Continuer' ;
  }
  stop() {
    this._timer. stop();
    this._state. setStop();
  }
  back() {
    this._timer. reset();
    this._state. setBack();
    this._btnPlay = 'Démarrer' ;
  }
}

export class Timer{
  private min: number=0;
  private sec: number=0;
  private totalSec: number=0;
  private _timer;
  get _min(): number{ return this. min; }
  get _sec(): number{ return this. sec; }
  start(){
    this. _timer=setInterval(() =>{
    this. min=Math. floor(++this. totalSec/60);
    this. sec=this. totalSec-this. min/60;
  }, 1000);
  }
  stop(){
   clearInterval(this. _timer);
  }
  reset(){
    this. min=this. sec=this. totalSec=0;
  }
 }
 export class State{
  private _play: boolean=true;
  private _stop: boolean=false;
  private _back: boolean=false;
  get stop(): boolean{ return this. _stop;}
  get play(): boolean{ return this. _play;}
  get back(): boolean{ return this. _back;}
  setStop(){
    this. _stop=false;
    this. _play=this. _back=true;
  }
  setPlay(){
    this. _stop = true;
    this. _play = this. _back = false;
  }
  setBack() {
    this. _play = true;
    this. _stop = this. _back = false;
  }
 }
